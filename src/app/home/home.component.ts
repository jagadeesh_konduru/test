import { Component, OnInit } from '@angular/core';
import { HomeService } from './home.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  categories: any;
  products: any;
  valid: boolean=false;

  constructor(private home:HomeService,private http: HttpClient) { }

  ngOnInit(): void {
  this.home.getcategories().subscribe((data:any) => {
  this.categories = data;
  console.log(data);
})
// this.getproducts();
  }

  getproducts(caatinfo:any){
    this.valid = false;
    this.home.getproducts(caatinfo).subscribe((data:any)=>{
      this.products = data;
      this.valid = true;
      console.log(data);
    })
  }

}
