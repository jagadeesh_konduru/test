import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http: HttpClient) { }

  getcategories(){
    return this.http.get('https://fakestoreapi.com/products/categories');
  }
  getproducts(cainfo:any){
    return this.http.get('https://fakestoreapi.com/products/category/' +cainfo);
  }
}
